//
//  ViewController.swift
//  SemaphoreDemo
//
//  Created by tdnhan on 10/16/20.
//  Copyright © 2020 Axon Vibe. All rights reserved.
//

import UIKit

let kMaxConcurrent = 3
let semaphore = DispatchSemaphore(value: kMaxConcurrent)
let downloadQueue = DispatchQueue(label: "com.app.downloadQueue", attributes: .concurrent)

class ViewController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func handleTap(_ sender: Any) {
        for i in 0..<15 {
            downloadQueue.async { [unowned self] in
                // Lock shared resource access
                semaphore.wait()
                
                // Expensive task
                self.download(i + 1)
                
                // Update the UI on main thread
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    // Release the lock
                    semaphore.signal()
                }
            }
        }
    }
    
    func download(_ songId: Int) {
        var counter = 0
        for _ in 0..<Int.random(in: 999999...10000000) {
            counter += songId
        }
        print("counter \(counter)")
        print("counter \(counter)")
    }
}

