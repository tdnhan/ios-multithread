import UIKit

//===================BlockOperation=====================

//let blockOperation1: BlockOperation = BlockOperation {
//    print("blockOperation 1 run")
//}
//let blockOperation2: BlockOperation = BlockOperation {
//    print("blockOperation 2 run")
//}
//
//let queue = OperationQueue()
//queue.addOperations([blockOperation1, blockOperation2], waitUntilFinished: true)


//====================CustomOperation================
//
//class PrintOperation: Operation {
//    let stringToPrint: String
//
//    init(stringToPrint: String = "Nothing to print") {
//        self.stringToPrint = stringToPrint
//        super.init()
//    }
//
//    override func main() {
//        guard !isCancelled else { return }
//        print(stringToPrint)
//    }
//}
//
//let queue = OperationQueue()
//let printOperation = PrintOperation(stringToPrint: "Hello, world!")
//printOperation.completionBlock = {
//    print("Print done!!!")
//}
//queue.addOperation(printOperation)


//====================Dependencies================

class FirstPrintOperation: Operation {
    let stringToPrint: String

    init(stringToPrint: String = "Nothing to print") {
        self.stringToPrint = stringToPrint
        super.init()
    }

    override func main() {
        guard !isCancelled else { return }
        for _ in 0...10 {
            sleep(1)
            print("ABC 1")
        }
        print(stringToPrint)
    }
}
//
//class SecondPrintOperation: Operation {
//    let stringToPrint: String
//
//    init(stringToPrint: String = "This isn't \"Hello, World\"") {
//        self.stringToPrint = stringToPrint
//        super.init()
//    }
//
//    override func main() {
//        guard !dependencies.contains(where: { $0.isCancelled }), !isCancelled else { return }
//        for _ in 0...10 {
//            sleep(1)
//            print("ABC 2")
//        }
//        print(stringToPrint)
//    }
//}
//
//let firstPrintOperation = FirstPrintOperation(stringToPrint: "FirstprintOperation")
//let secondPrintOperation = SecondPrintOperation(stringToPrint: "SecondPrintOperation")
////secondPrintOperation.addDependency(firstPrintOperation)
//
//let multipleOprationsQueue = OperationQueue()
//multipleOprationsQueue.addOperations([firstPrintOperation, secondPrintOperation], waitUntilFinished: true)

//================Passing data between dependencies================

struct UserModel: Codable {
    var userId: Int
    var id: Int
    var title: String
    var completed: Bool
}

class FetchOpration: Operation {
    let data = """
    {
    "userId": 1,
    "id": 2,
    "title": "test",
    "completed": true
    }
    """
    private(set) var dataFetched: Data?

    override func main() {
        self.dataFetched = data.data(using: .utf8)
    }
}

//final class DecodeOperation: Operation {
//    var dataFetched: Data?
//    private(set) var jsonParsed: [String: Any]?
//
//    typealias CompletionHandler = (_ result: UserModel?) -> Void
//
//    var completionHandler: CompletionHandler?
//
//    override func main() {
//        guard let dataFetched = dataFetched else { return }
//        let decoder = JSONDecoder()
//        let content = try? decoder.decode(UserModel.self, from: dataFetched)
//        completionHandler?(content)
//    }
//}
//
//let fetchOperation = FetchOpration()
//let decodeOperation = DecodeOperation()
//
//let adapter = BlockOperation { [unowned fetchOperation, unowned decodeOperation] in
//    decodeOperation.dataFetched = fetchOperation.dataFetched
//}
//
//adapter.addDependency(fetchOperation)
//decodeOperation.addDependency(adapter)
//
//decodeOperation.completionHandler = { decodeData in
//    print("decode:\n\(decodeData)")
//}
//
//let operationQueue = OperationQueue()
//operationQueue.addOperations([fetchOperation, decodeOperation, adapter], waitUntilFinished: true)


