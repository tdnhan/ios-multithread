import UIKit
import PlaygroundSupport
import Foundation
//==================Concurrency queue=====================

//let concurrentQueue = DispatchQueue(label: "concurrentQueue", qos: .default, attributes: .concurrent)
//concurrentQueue.async {
//    for i in 1...4 {
//        print("concurrentQueue 1 - task \(i)")
//    }
//}
//
//concurrentQueue.async {
//    for i in 1...4 {
//        print("concurrentQueue 2 - task \(i)")
//    }
//}

//=================Serial queue==========================

//let serialQueue = DispatchQueue(label: "serialQueue", qos: .unspecified, attributes: [], autoreleaseFrequency: .inherit, target: nil)
//let serialQueue = DispatchQueue(label: "serialQueue")
//
//serialQueue.async {
//    for i in 1...4 {
//        print("serialQueue 1 - task \(i)")
//    }
//}
//
//serialQueue.async {
//    for i in 1...4 {
//        print("serialQueue 2 - task \(i)")
//    }
//}

//==================Main queue============================
//let mainQueue = DispatchQueue.main
//
//mainQueue.async {
//    for i in 1...4 {
//        print("mainQueue 1 - task \(i)")
//    }
//}
//
//mainQueue.async {
//    for i in 1...4 {
//        print("mainQueue 2 - task \(i)")
//    }
//}
//================Global queue=============================

//let globalQueue = DispatchQueue.global(qos: .default)
//print(Thread.current)
//globalQueue.async {
//    print(Thread.current)
//    for i in 1...4 {
//        print("globalQueue 1 - task \(i)")
//    }
//    DispatchQueue.global().async {
//        print(Thread.current)
//    }
//    print("end thread")
//}

//globalQueue.sync {
//    print(Thread.current)
//    for i in 1...4 {
//        print("globalQueue 2 - task \(i)")
//    }
//}
//=================Custom queue================================

//let customQueue = DispatchQueue(label: "queue", qos: .default, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
//DispatchQueue.main.async {
//    print(Thread.current)
//
//    DispatchQueue.main.async {
//        print(Thread.current)
//    }
//    print("asdasdas")
//}
//customQueue.sync {
//    print(Thread.current)
//    for _ in 1...10 {
//        print("thread 1")
//    }
//}
//
//customQueue.sync {
//    for _ in 1...10 {
//        print("thread 2")
//    }
//}
//===================Delay=================================
//
//print("start do task at \(Date())")
//let deadline = DispatchTime.now() + .seconds(3)
//DispatchQueue.main.asyncAfter(deadline: deadline) {
//    print("Finish task at \(Date())")
//}
//==================DispatchWorkItem====================

//var workItem = DispatchWorkItem {
//    print(Thread.isMainThread)
//    print("Do the work item!!!")
//}
// Run in main thread
//workItem.perform()
// Run in dispatch queue
//DispatchQueue.global(qos: .background).sync(execute: workItem)
//DispatchQueue.global().async {
//    workItem.perform()
//}

// Barrier
//var barrierWorkItem = DispatchWorkItem(qos: .default, flags: .barrier) {
//    for i in 1...10 {
//        print("barrier task \(i)")
//    }
//}
//let queue = DispatchQueue(label: "barrier-test-queue", qos: .default, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
//let serial = DispatchQueue(label: "serialQueue")
//
//queue.async {
//    for i in 0...10 {
//        print("A \(i)")
//    }
//}
//queue.async(execute: barrierWorkItem)
//queue.sync {
//    print("B")
//    sleep(1)
//}
//print("done")

// Cancel
//let queue = DispatchQueue.global(qos: .background)
//func work(task: DispatchWorkItem?) {
//    if task?.isCancelled == false {
//        print("Start working")
//    }
//    sleep(2)
//    if task?.isCancelled == true {
//        print("Work is cancelled")
//    }
//    print("Finish work")
//}
//var task: DispatchWorkItem?
//let item = DispatchWorkItem {
//    work(task: task)
//}
//task = item
//task?.notify(queue: queue, execute: {
//    print("Work is notify")
//})
//queue.async(execute: task!)
// Wait
//task?.wait()
//queue.asyncAfter(deadline: .now() + 1) {
//    print("Cancel work")
//    task?.cancel()
//}
// Notify

//==================Dispatch Group====================

//let dispatchGroup = DispatchGroup()
//// (1)
//let task1 = DispatchWorkItem {
//    sleep(1)
//    print("Task1 is finished")
//}
//DispatchQueue(label: "task1").async(group: dispatchGroup, execute: task1)
//// (2)
//let task2 = DispatchWorkItem {
//    sleep(2)
//    print("Task2 is finished")
//}
//DispatchQueue(label: "task2").async {
//    dispatchGroup.enter()
//    task2.perform()
//    dispatchGroup.leave()
//}
////dispatchGroup.notify(queue: DispatchQueue.main, work: task1)
//dispatchGroup.notify(queue: DispatchQueue.main) {
//    print("group is done")
//}
//// Wait
//func roof() -> Bool {
//    print("roof start")
//    (0...10000000).forEach { _ in }
//    print("rooft end")
//    return true
//}
//
//for _ in 1...5 {
//    dispatchGroup.enter()
//    DispatchQueue.global().async {
//        if roof() { dispatchGroup.leave() }
//    }
//}
//let result = dispatchGroup.wait(timeout: .now() + 5)
//print("dispatchGroup wait \(result)")

//==================Dispatch Barrier================
// (1)
//PlaygroundPage.current.needsIndefiniteExecution = true
//
//var value: Int = 2
//let concurrentQueue = DispatchQueue(label: "queue", attributes: .concurrent)
//
//concurrentQueue.async {
//    for i in 0...3 {
//        value = i
//        print("\(value) ✴️")
//    }
//}
//
//concurrentQueue.async {
//    for j in 4...6 {
//        value = j
//        print("\(value) ✡️")
//    }
//}
//
//concurrentQueue.async {
//    value = 9
//    print(value)
//}
//
//concurrentQueue.async {
//    value = 14
//    print(value)
//}
//
// (2)
//concurrentQueue.async(flags: .barrier) {
//    for i in 0...3 {
//        value = i
//        print("\(value) ✴️")
//    }
//}
//
//concurrentQueue.async {
//    print(value)
//}
//
//concurrentQueue.async(flags: .barrier) {
//    for j in 4...6 {
//        value = j
//        print("\(value) ✡️")
//    }
//}
//
//concurrentQueue.async {
//    value = 9
//    print(value)
//}
//
//concurrentQueue.async {
//    value = 14
//    print(value)
//}

//=======================DispatchSemaphore==================

//PlaygroundPage.current.needsIndefiniteExecution = true
//
//var value: Int = 2
//
//let concurrentQueue = DispatchQueue(label: "queue", attributes: .concurrent)
//let semaphore = DispatchSemaphore(value: 1)
//
//for j in 0...4 {
//    concurrentQueue.async {
//        print("\(j) waiting")
//        semaphore.wait()
//        print("\(j) wait finished")
//        value = j
//        print("\(value) ✡️")
//        print("\(j) Done with assignment")
//        semaphore.signal()
//    }
//}

//func powerSum(X: Int, N: Int, num: Int) -> Int {
//    let value = X - Int(pow(Float(num), Float(N)))
//    if value < 0 { return 0 }
//    if value == 0 { return 1 }
//    return powerSum(X: X, N: N, num: num+1) + powerSum(X: value, N: N, num: num+1)
//}
//
//func maxSubarray(arr: [Int]) -> [Int] {
//    var maxSubsequency = arr[0]
//    var curSubArray = arr[0]
//    var maxSubArray = arr[0]
//    for i in 1..<arr.count {
//        curSubArray = [curSubArray + arr[i], arr[i]].max()!
//        maxSubArray = [curSubArray, maxSubArray].max()!
//
//        let value = arr[i] + maxSubsequency
//        maxSubsequency = [maxSubsequency, value, arr[i]].max()!
//    }
//    return [maxSubArray, maxSubsequency]
//}
//
//func simpleArraySum(ar: [Int]) -> Int {
//    var sum = 0
//    for num in ar {
//        sum += num
//    }
//    return sum
//}
//
//func compareTriplets(a: [Int], b: [Int]) -> [Int] {
//    if a.count != b.count { return [] }
//    var sumA = 0
//    var sumB = 0
//    for i in 0..<a.count {
//        switch a[i] - b[i] {
//        case let x where x > 0:
//            sumA += 1
//        case let x where x < 0:
//            sumB += 1
//        default:
//            break
//        }
//    }
//    return [sumA, sumB]
//}
//
//func fibonacci(n: Int) -> Int {
//    var f0 = 0
//    var f1 = 1
//    var result = 0
//    for _ in 1..<n {
//        result = f0 + f1
//        f0 = f1
//        f1 = result
//    }
//    return result
//}
//
//func sumArray(ar: [Int]) -> Int {
//    var sum = 0
//    ar.forEach { s in
//        sum += s
//    }
//    return sum
//}
//
//
//func sockMerchant(n: Int, ar: [Int]) -> Int {
//    var sum = 0
//    var arrPass = ar
//    for i in 0..<n-1 {
//        if i > arrPass.count-1 { break }
//        let tmp = arrPass[i]
//        for j in i+1..<arrPass.count {
//            if tmp == arrPass[j] {
//                sum += 1
//                arrPass.remove(at: j)
//                break
//            }
//        }
//    }
//    return sum
//}
//
//func countingValleys(steps: Int, path: String) -> Int {
//    let arr = Array(path)
//    guard steps == arr.count, !arr.contains(where: { $0.uppercased() != "U" && $0.uppercased() != "D" }) else { return 0 }
//    var result = 0, count = 0
//    var catchDown = false
//    for step in arr {
//        switch step {
//        case "U":
//            count += 1
//        case "D":
//            count -= 1
//        default:
//            break
//        }
//        if count < 0 {
//            catchDown = true
//        }
//        if count == 0, catchDown == true {
//            result += 1
//            catchDown = false
//        }
//    }
//    return result
//}
//
//func jumpingOnClouds(c: [Int]) -> Int {
//    var jumpCount = 0
//    var cur = 0
//    let count = c.count
//
//    while cur < count - 1 {
//        if cur + 2 < count, c[cur + 2] == 0 {
//            cur += 2
//        } else {
//            cur += 1
//        }
//        jumpCount += 1
//    }
//    return jumpCount
//}
//
//func repeatString(s: String, n: Int) -> Int {
//    let aChar = Array(s).filter({ $0 == "a" }).count
//    let times = n / s.count
//    let rest = n % s.count
//
//    let total = times * aChar + s.prefix(rest).filter({ $0 == "a" }).count
//    return total
//}
//
func hourglassSum(arr: [[Int]]) -> Int {
    var count = -9 * 7
    for i in 0..<4 {
        for j in 0..<4 {
            let top = arr[i][j] + arr[i][j+1] + arr[i][j+2]
            let middle = arr[i+1][j+1]
            let bottom = arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]
            let sum = top + middle + bottom

            if sum > count {
                count = sum
            }
        }
    }
    return count
}

func rotLeft(a: [Int], d: Int) -> [Int] {
    guard d <= a.count else { return a }
    var tmp = a
    var dTmp = d
//    let left = a[0..<d]
//    tmp.removeFirst(d)
//    tmp.append(contentsOf: left)
    while dTmp > 0 {
        tmp.append(tmp.removeFirst())
        dTmp -= 1
    }
//    let n = a.count
//    for i in 0 ..< n {
//        tmp[i] = a[(i + d)%n]
//    }

    return tmp
}
//
//func minimumBribes(q: [Int]) {
//    var chaotic = false
//    var bribes = 0
//    var last = 0
//    var j = 0
//    var tmp = q
//    if q[0] - 1 > 2 {
//        chaotic = true
//    }
//    if chaotic == false {
//        for i in 1..<tmp.count {
//            if q[i] - (i + 1) > 2 {
//                chaotic = true
//                break
//            }
//            last = tmp[i]
//            j = i
//            while j > 0 && tmp[j-1] > last {
//                tmp[j] = tmp[j-1]
//                j = j - 1
//
//                bribes += 1
//            }
//            tmp[j] = last
//        }
//    }
//
//    if chaotic {
//        print("Too chaotic")
//    } else {
//        print(bribes)
//    }
//}
//
//func minimumSwaps(arr: [Int]) -> Int {
////    var result = 0
////    var arrTmp = arr
////    for i in 0..<arrTmp.count-1 {
////        if arrTmp[i] == i + 1 { continue }
////        var min = i
////        for j in i+1..<arrTmp.count where arrTmp[j] < arrTmp[min] {
////            min = j
////        }
////
////        if i != min {
////            let tmp = arrTmp[i]
////            arrTmp[i] = arrTmp[min]
////            arrTmp[min] = tmp
////            result += 1
////        }
////    }
////    return result
//    var result = 0
//    var i = 0
//    var arrTmp = arr
//    while i < arrTmp.count {
//        defer {
//            i += 1
//        }
//        if arrTmp[i] == i + 1 { continue }
//        result += 1
//        let tmp = arrTmp[i]
//        arrTmp[i] = arrTmp[tmp - 1]
//        arrTmp[tmp - 1] = tmp
//        i -= 1
//    }
//    return result
//}
//
//func quickSort<T: Comparable>(arr: [T]) -> [T] {
//    guard arr.count > 1 else { return arr }
//
//    let pivot = arr[arr.count / 2]
//    let less = arr.filter({ $0 < pivot })
//    let equal = arr.filter({ $0 == pivot })
//    let greater = arr.filter({ $0 > pivot })
//
//    return quickSort(arr: less) + equal + quickSort(arr: greater)
//}
//
//func arrayManipulation(n: Int, queries: [[Int]]) -> Int {
////    var result = 0
////    for i in 0..<n {
////        var tmp = 0
////        for query in queries where i >= query[0] - 1 && i < query[1] {
////            tmp += query[2]
////        }
////        if result < tmp {
////            result = tmp
////        }
////    }
////    return result
//    var arr = [Int](repeating: 0, count: n)
//    for i in queries {
//        arr[i[0] - 1] += i[2]
//        if i[1] != arr.count {
//            arr[i[1]] -= i[2]
//        }
//    }
//    var max = 0, itt = 0
//    print(arr)
//    for t in arr {
//        itt += t
//        if itt > max {
//            max = itt
//        }
//    }
//    return max
//}
//
//func checkMagazine(magazine: [String], note: [String]) {
//    guard magazine.count >= note.count else {
//        print("No")
//        return
//    }
//    var magazineDict = [String: Int]()
//    for word in magazine {
//        if magazineDict[word] != nil {
//            magazineDict[word]! += 1
//        } else {
//            magazineDict[word] = 1
//        }
//    }
//    // Check magazine with ransom note
//    for word in note {
//        if magazineDict[word] != nil {
//            magazineDict[word]! -= 1
//            if magazineDict[word]! < 0 {
//                print("No")
//                return
//            }
//        } else {
//            print("No")
//            return
//        }
//    }
//    print("Yes")
//}

func binaryNumbers(n: Int) -> Int {
    var sum = 0, max = 0
    var tmp = n
    while tmp > 0 {
        if tmp % 2 == 1 {
            sum += 1
            if sum > max {
                max = sum
            }
        } else {
            sum = 0
        }
        
        tmp /= 2
    }
    return max
}

func solve(meal_cost: Double, tip_percent: Int, tax_percent: Int) {
    let result = Int((meal_cost * (100.0 + Double(tip_percent) + Double(tax_percent)) / 100.0).rounded())
    print(result)
}

//class Person {
//    var age: Int = 0
//
//    init(initialAge: Int) {
//        guard initialAge >= 0 else {
//            print("Age is not valid, setting age to 0.")
//            return
//        }
//        self.age = initialAge
//    }
//
//    func amIOld() {
//        switch age {
//        case 0..<13:
//            print("You are young.")
//        case 13..<18:
//            print("You are a teenager.")
//        default:
//            print("You are old.")
//        }
//    }
//
//    func yearPasses() {
//        age += 1
//    }
//}

func printEvenAndOdd(string: String) {
    var even = ""
    var odd = ""
    var index = 0
    for i in string {
        if index%2 == 0 {
            even += "\(i)"
        } else {
            odd += "\(i)"
        }
        index += 1
    }
    print("\(even) \(odd)")
}

class Person {
    private let firstName: String
    private let lastName: String
    private let id: Int

    // Initializer
    init(firstName: String, lastName: String, id: Int) {
        self.firstName = firstName
        self.lastName = lastName
        self.id = id
    }

    // Print person data
    func printPerson() {
        print("Name: \(lastName), \(firstName)")
        print("ID: \(id)")
    }
}

class Student: Person {
    var testScores: [Int]
    
    init(firstName: String, lastName: String, id: Int, scores: [Int]) {
        self.testScores = scores
        super.init(firstName: firstName, lastName: lastName, id: id)
    }
    
    func calculate() -> Character {
        let average = testScores.reduce(0, { $0 + $1 }) / testScores.count
        switch average {
        case 90...100:
            return "O"
        case 80..<90:
            return "E"
        case 70..<80:
            return "A"
        case 55..<70:
            return "P"
        case 40..<55:
            return "D"
        case 0..<40:
            return "T"
        default:
            return "T"
        }
    }
}

class Difference {
    private var elements = [Int]()
    var maximumDifference: Int
    
    init(a: [Int]) {
        self.elements = a
        maximumDifference = 0
    }
    
    func computeDifference() {
        for i in 0..<elements.count - 1 {
            let value = elements[i]
            for j in (i+1)..<elements.count {
                let tmp = abs(value - elements[j])
                if tmp > maximumDifference {
                    maximumDifference = tmp
                }
            }
        }
    }
}

class Solution {
    var stack: [Character] = []
    var stackSize = 0
    var queue: [Character] = []
    
    func pushCharacter(ch: Character) {
        stack.append(ch)
        stackSize += 1
    }
    
    func popCharacter() -> Character {
        stackSize -= 1
        return stack.remove(at: stackSize)
    }
    
    func enqueueCharacter(ch: Character) {
        queue.append(ch)
    }
    
    func dequeueCharacter() -> Character {
        return queue.removeFirst()
    }
}

struct Printer<T> {
    func printArray<T>(array: [T]) {
        for elm in array {
            print(elm)
        }
    }
}

func isPrime(_ n: Int) -> Bool {
    if n == 1 || (n > 2 && n % 2 == 0) { return false }
    if n == 2 { return true }
    for i in stride(from: 3, through: Int(sqrt(Double(n))), by: 2) {
        if n % i == 0 {
            return false
        }
    }
    
    return true
}

var list = [String]()

let N = Int(readLine()!)!

for _ in 0 ..< N {
    let input = readLine()!.split(separator: " ").map(String.init)

    let name = input[0]
    let email = input[1]
    if email.range(of: ".+@gmail\\.com$", options: .regularExpression) != nil {
        list.append(name)
    }
}

list.sort()

list.forEach({ name in
    print(name)
})

func reverseArray(a: [Int]) -> [Int] {
    var arr = [Int]()
    let lastIndex = a.count - 1
    for i in 0...lastIndex {
        arr.append(a[lastIndex - i])
    }
    return arr
}

func dynamicArray(n: Int, queries: [[Int]]) -> [Int] {
    var lastAnswer = 0
    var seq = Array(repeating: [Int](), count: n)
    var result = [Int]()
    
    for i in queries {
        let type = i[0]
        let x = i[1]
        let y = i[2]
        let index = (x ^ lastAnswer) % n
        if type == 1 {
            seq[index].append(y)
        } else {
            // Calculate the size of the sequnce at this index
            let size = seq[index].count
            // Look up the value in this sequence at index y % size
            lastAnswer = seq[index][y % size]
            result.append(lastAnswer)
        }
    }
    return result
}

func matchingStrings(strings: [String], queries: [String]) -> [Int] {
    var map = [String: Int]()
    var res = Array(repeating: 0, count: queries.count)
    for str in strings {
        map[str] = (map[str] ?? 0) + 1
    }
    for i in 0..<queries.count {
        if let count = map[queries[i]] {
            res[i] = count
        } else {
            res[i] = 0
        }
    }
    return res
}
